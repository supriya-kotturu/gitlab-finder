class GitLab {
    constructor() {
        this.personal_access_token = "hye-xxAJMgijipzATqbA";
    }

    async getUser(user) {
        let profileURL = `https://gitlab.com/api/v4/users?username=${user}`;
        let repoURL = `https://gitlab.com/api/v4/users/${user}/projects`;

        const profileResponse = await fetch(profileURL);
        const profileData = await profileResponse.json();

        const repoResponse = await fetch(repoURL, {
            method: 'GET',
            headers: {
                'Private-Token': this.personal_access_token
            }
        });
        const repoData = await repoResponse.json();

        return {
            profileData,
            repoData
        };
    }

    async getProject(project) {
        let projectURL = `https://gitlab.com/api/v4/search?scope=projects&search=${project}`;
        // let projectMembersURL = `https://gitlab.com/api/v4/projects/${project}/members`;

        const projectResponse = await fetch(projectURL, {
            method: 'GET',
            headers: {
                'Private-Token': this.personal_access_token
            }
        });
        const projectData = await projectResponse.json();
        return {
            projectData
        };
    }

    async getGroup(group) {
        let groupURL = `https://gitlab.com/api/v4/groups/${group}?with_projects=true`;
        // let groupMembersURL = `https://gitlab.com/api/v4/projects/${group}/members`;

        const groupResponse = await fetch(groupURL, {
            method: 'GET',
            headers: {
                'Private-Token': this.personal_access_token
            }
        });
        // const groupMemberResponse = await fetch(groupMembersURL,
        // {
        //     method: 'GET',
        //     headers: {
        //         'Private-Token': this.personal_access_token
        //     }
        // });

        const groupData = await groupResponse.json();
        // const groupMemData = await groupMemberResponse.json();
        return {
            groupData
            // groupMemData
        };
    }

}