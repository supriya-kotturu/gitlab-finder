const userInput = document.querySelector("#user-input");
const from = document.querySelector("#from");
const results = document.querySelector("#results");

const gitlab = new GitLab();
const ui = new UI();

userInput.addEventListener("keyup", (e) => {
    let userText = e.target.value;
    let option = from.value;
    if (userText !== '') {
        switch (option.toLowerCase()) {
            case "users":
                gitlab.getUser(userText)
                    .then(data => {
                        if (data.profileData.length !== 0 || data.repoData.message !== ('404 User Not Found')) {
                            ui.showProfile(data);
                        } else {
                            ui.showAlert("users", `uh-oh, we coudn't find them :(`, userText, "alert");
                        }
                    })
                    .catch(err => console.log(err));
                break;
            case "projects":
                gitlab.getProject(userText)
                    .then(data => {
                        if (data.projectData.length !== 0) {
                            ui.showProjects(data.projectData);
                        } else {
                            ui.showAlert("projects", `Oh-oh!`, userText, "alert");
                        }
                    })
                    .catch(err => console.log(err));
                break;
            case "groups":
                gitlab.getGroup(userText)
                    .then(data => {
                        let group = data.groupData;
                        if (group.message !== "404 Group Not Found") {
                            ui.showGroups(group);
                        } else {
                            ui.showAlert("groups", "yikes!", userText, "alert");
                        }
                    })
                    .catch(err => console.log(err));
                break;
            default:
                ui.showAlert("scope","Not so fast, kiddo!","wosh","alert");
        }
    } else {
        //remove the contents in #results
        while (results.firstChild) {
            results.removeChild(results.firstChild);
        }
    }
});