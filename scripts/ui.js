class UI {
    constructor() {
        this.results = document.querySelector("#results");
        this.repos = document.querySelector("#repos");
    }

    showProfile(usersData) {
        let profiles = usersData.profileData;
        let repos = usersData.repoData;
        let projectsLable = ``;
        let profileUI = '',
            reposUI = '';
        let result = '';
        results.innerHTML = "";
        profiles.forEach(profile => {
            profileUI += `<div class="col s10 m4 offset-m4 offset-s1">
            <figure class="center">
                <img src=${profile.avatar_url} class="responsive-img circle" alt=${profile.name}>
                <figcaption>
                    <h4 class="thin russian-blue-text">${profile.name}</h4>
                </figcaption>
            </figure>
            <a href=${profile.web_url} target="_blank" class="btn-large btn-block">view profile</a></div>`;
            if (repos.length === 0) {
                projectsLable = `<div class="row">
                <div class="col s12">
                <br><br>
                <h4 class="thin center red-text text-darken-4">Oops! couldn't find any public repos of ${profile.name}</h4>
                </div>
                </br></div>`;
            } else {
                projectsLable = `<div class="row">
                <div class="col s12">
                <br><br>
                    <h4 class="thin center russian-blue-text">Take a peek at  ${profile.name}'s contributions.</h4>
                </div>
                </br></div>`;
                repos.forEach(repo => {
                    if (repo.avatar_url === null) {
                        repo.avatar_url = "static/gitlab3.png"
                    }
                    reposUI += `<div class="row"><div class="col s12 l10 offset-l1">
                    <div class="card horizontal russian-blue-text">
                        <div class="card-image hide-on-small-only">
                        <img src=${repo.avatar_url} style = "max-height : 210px; max-width = 180px" alt=${repo.name}>
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <h5 class = "russian-blue-text">${repo.name}</h5>
                                <p class = "russian-blue-text">${repo.description}</p>
                            </div>
                            <div class="card-action">
                            <span class = "left">
                            <a href="http://" target="_blank" rel="forks"><img src="static/icons/icons8-code-fork-50.png" alt="fork" height="20px" >${repo.forks_count}</a>
                            <a href="http://" target="_blank" rel="star count"><img src="static/icons/icons8-star-half-empty-50.png" alt="star" height="20px">${repo.star_count}</a>
                            </span>
                            <a href=${repo.web_url} class="btn right" target="_blank">repo link</a>
                            </div>
                        </div>
                    </div></div>`
                });
            }
        });

        result = profileUI + projectsLable + reposUI;
        this.results.innerHTML = result;
    }

    showProjects(repos) {
        let reposUI = '';
        repos.forEach(repo => {
            if (repo.avatar_url === null) {
                repo.avatar_url = "static/gitlab3.png"
            }
            reposUI += `<div class="row"><div class="col s12 l10 offset-l1">
            <div class="card horizontal russian-blue-text">
                <div class="card-image hide-on-small-only">
                <img src=${repo.avatar_url} style = "max-height : 210px; max-width = 180px" alt=${repo.name}>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h5 class = "russian-blue-text">${repo.name}</h5>
                        <p class = "orange-soda-text">${repo.namespace.name}</p>
                        <p class = "russian-blue-text">${repo.description}</p>
                    </div>
                    <div class="card-action">
                    <span class = "left">
                    <a href="http://" target="_blank" rel="forks"><img src="static/icons/icons8-code-fork-50.png" alt="fork" height="20px" >${repo.forks_count}</a>
                    <a href="http://" target="_blank" rel="star count"><img src="static/icons/icons8-star-half-empty-50.png" alt="star" height="20px">${repo.star_count}</a>
                    </span>
                    <a href=${repo.web_url} class="btn right" target="_blank">repo link</a></div>
                </div>
            </div></div>`;
        });
        this.results.innerHTML = reposUI;
    }

    showGroups(group) {
        let groupProjects = group.projects;
        let groupUI = '',
            projectsUI = '';
        if (group.avatar_url === null) {
            group.avatar_url = "static/gitlab2.png"
        }
        groupUI += `<div class="row"><div class="col s12">
        <div class="card horizontal russian-blue">
            <div class="card-image hide-on-small-only">
            <img src=${group.avatar_url} style = "max-height : 240px; max-width = 240px" alt=${group.name}>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h5 class = "white-text">${group.name}</h5>
                    <p class = "white-text">${group.description}</p>
                </div>
                <div class="card-action">
                <a href=${group.web_url} class="btn right" target="_blank">View group</a></div>
            </div>
        </div></div>`;
        if (groupProjects.length === 0) {
            projectsUI = `<div class="row">
            <div class="col s12">
            <br><br>
            <h4 class="thin center red-text text-darken-4">Jinkies! no public repositories under ${group.name}</h4>
            </div>
            </br>`;
            // no ending </div> for class="row" it will be added at the end
        } else {
            projectsUI += `<div class="row">
            <div class="col s12">
            <br><br>
                <h4 class="thin center russian-blue-text">Explore ${group.name}'s projects to contribute.</h4>
            </div>
            </br></div>`;
            groupProjects.forEach(project => {
                if (project.avatar_url === null) {
                    project.avatar_url = "static/gitlab3.png"
                }
                projectsUI += `<div class="col s10 m6 l4 offset-s1">
                <div class="card small">
                    <div class="card-content russian-blue-text">
                        <img src=${project.avatar_url} style="max-height : 50px; max-width : 50px" alt='${project.name}'>
                        <span class="card-title russian-blue-text ">${project.name}</span>
                        <p class="russian-blue-text">${project.description}</p>
                    </div>
                    <div class="card-action">
                    <span class = "left">
                    <a href="http://" target="_blank" rel="forks"><img src="static/icons/icons8-code-fork-50.png" alt="fork" height="20px" >${project.forks_count}</a>
                    <a href="http://" target="_blank" rel="star count"><img src="static/icons/icons8-star-half-empty-50.png" alt="star" height="20px">${project.star_count}</a>
                    </span>
                    <a href=${project.http_url_to_repo} class="btn right" target="_blank">Repo link</a></div>
                </div></div>`;
            });
        }
        this.results.innerHTML = groupUI + projectsUI + '</div>';
    }

    showAlert(scope, message, userIP, className) {
        this.clearAlert();
        const div = document.createElement('div');
        div.className = className;
        switch (scope) {
            case "users":
                div.innerHTML = `<div class="row">
                <div class="col s10 m6 offset-m3 offset-s1 l6 offset-l3">
                <div class="card red darken-4 ">
                <div class="card-content white-text">
                <span class="card-title center">${message}</span>
                <p class="center"> No account found with username: ${userIP}</p>
                </div>
                </div>
                </div>`;
                break;
            case "projects":
                div.innerHTML = `<div class="row">
                <div class="col s10 m6 offset-m3 offset-s1 l6 offset-l3">
                <div class="card red darken-4 ">
                <div class="card-content white-text">
                <span class="card-title center">${message}</span>
                <p class="center"> No project found with name: ${userIP}</p>
                </div>
                </div>
                </div>`;
                break;
            case "groups":
                div.innerHTML = `<div class="row">
                <div class="col s10 m6 offset-m3 offset-s1 l6 offset-l3">
                <div class="card red darken-4 ">
                <div class="card-content white-text">
                <span class="card-title center">${message}</span>
                <p class="center"> No group found with name: ${userIP}</p>
                </div>
                </div>
                </div>`;
                break;
            default:
                div.innerHTML = `<div class="row">
                <div class="col s10 m6 offset-m3 offset-s1 l6 offset-l3">
                <div class="card red darken-4 ">
                <div class="card-content white-text">
                <span class="card-title center">${message}</span>
                <p class="center">Select the scope you want to search</p>
                </div>
                </div>
                </div>`;
                break;
        }

        const container = document.querySelector("#results-container");
        const results = document.getElementById("results");
        container.insertBefore(div, results);

        setTimeout(() => {
            this.clearAlert()
        }, 2500);
    }

    clearAlert() {
        const currentAlert = document.querySelector('.alert');
        if (currentAlert) {
            currentAlert.remove();
        }
    }
}