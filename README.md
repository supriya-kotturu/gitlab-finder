# GitLab finder

Explore users, public groups and projects from GitLab. Get started by selecting the scope from the dropdown and enter the username, project or group ID. GitLab finder uses GitLab API to fetch your details.

Try it out: https://supriya-kotturu.gitlab.io/gitlab-finder 